<!-- Details Modal -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="detailModalLabel">Contact Details</h4>
            </div>
            <div class="modal-body">
                <h3 id="contact-id"></h3>
                <table class="table table-bordered">
                    <tr>
                        <th>First Name:</th>
                        <td id="contact-firstName"></td>
                    </tr>
                    <tr>
                        <th>Last Name:</th>
                        <td id="contact-lastName"></td>
                    </tr>
                    <tr>
                        <th>Street Address:</th>
                        <td id="contact-streetAddress"></td>
                    </tr>
                    <tr>
                        <th>City:</th>
                        <td id="contact-city"></td>
                    </tr>
                    <tr>
                        <th>State:</th>
                        <td id="contact-state"></td>
                    </tr>
                    <tr>
                        <th>Zipcode:</th>
                        <td id="contact-zipcode"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="editModalLabel">Edit Contact</h4>
            </div>
            <div class="modal-body">
                <h3 id="contact-id"></h3>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="edit-firstName" class="col-md-4 control-label">First Name:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-firstName" placeholder="First Name" /></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-lastName" class="col-md-4 control-label">Last Name:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-lastName" placeholder="Last Name" /></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-streetAddress" class="col-md-4 control-label">Street Address:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-streetAddress" placeholder="Street Address" /></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-city" class="col-md-4 control-label">City:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-city" placeholder="City" /></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-state" class="col-md-4 control-label">State:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-state" placeholder="State" /></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-zipcode" class="col-md-4 control-label">Zipcode:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-zipcode" placeholder="Zipcode" /></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit" id="edit-button" class="btn btn-default" data-dismiss="modal">Edit Contact</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <input type="hidden" id="contact-id"/>
                        </div>
                    </div>

                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">&times;</span>
                                        <span class="sr-only">Close</span>
                                    </button>
                                    <h4 class="modal-title" id="deleteModalLabel">Delete Contact</h4>
                                </div>
                                <div class="modal-body">
                                    <h3 id="contact-id"></h3>
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label for="delete-firstName" class="col-md-4 control-label">First Name:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-firstName" placeholder="First Name" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="delete-lastName" class="col-md-4 control-label">Last Name:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-lastName" placeholder="Last Name" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="delete-streetAddress" class="col-md-4 control-label">Street Address:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-streetAddress" placeholder="Street Address" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="delete-city" class="col-md-4 control-label">City:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-city" placeholder="City" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="delete-state" class="col-md-4 control-label">State:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-state" placeholder="State" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="delete-zipcode" class="col-md-4 control-label">Zipcode:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-zipcode" placeholder="Zipcode" /></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-offset-4 col-md-8">
                                                <button type="submit" id="-button" class="btn btn-default" data-dismiss="modal">Delete Contact</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <input type="hidden" id="contact-id"/>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
