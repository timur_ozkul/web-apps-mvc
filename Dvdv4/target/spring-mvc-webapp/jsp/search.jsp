<%-- 
    Document   : search
    Created on : Oct 28, 2015, 11:38:12 AM
    Author     : apprentice
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/icon.png" rel="shortcut icon">
    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
            <hr/>
            <div class ="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <h2>DVD List</h2>
                <%@include file="dvdSummaryTableFragment.jsp"%>
            </div> <!-- end col-md-6 div w/ table -->
            <div class="col-md-5">
                <h2>Search</h2>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="search-dvdTitle" class="col-md-4 control-label">DVD Title:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-dvdTitle" placeholder="DVD Title"/></div>
                    </div>
                    <div class="form-group">
                        <label for="search-releaseDate" class="col-md-4 control-label">Release Date:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-releaseDate" placeholder="Release Date" /></div>
                    </div>
                    <div class="form-group">
                        <label for="search-mpaaRating" class="col-md-4 control-label">Mpaa Rating:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-mpaaRating" placeholder="Mpaa Rating" /></div>
                    </div>
                    <div class="form-group">
                        <label for="search-notes" class="col-md-4 control-label">Notes:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-notes" placeholder="Notes"/></div>
                    </div>
                    <div class="form-group">
                        <label for="search-director" class="col-md-4 control-label">Director:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-director" placeholder="Director" /></div>
                    </div>
                    <div class="form-group">
                        <label for="search-studio" class="col-md-4 control-label">Studio:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-studio" placeholder="Studio" /></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8"><button type="submit" id="search-button" class="btn btn-default">Search DVD</button></div>
                    </div>
                </form>
            </div>
        </div>

        <%@include file="detailEditModalFragment.jsp"%>

        <!-- placed at the end of the document so that the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/dvdList.js"></script>
    </body>
</html>
