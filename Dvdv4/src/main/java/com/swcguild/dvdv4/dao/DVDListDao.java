/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdv4.dao;


import com.swcguild.dvdv4.model.DVD;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface DVDListDao {

    public DVD addDVD(DVD dvd);

    public void removeDVD(int dvdId);

    public void updateDVD(DVD dvd);

    public List<DVD> getAllDVD();

    public DVD getDVDById(int dvdId);

    public List<DVD> searchDVD(Map<SearchTerm, String> criteria);

}
