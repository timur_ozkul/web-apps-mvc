/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.vendingmachine.dto;

import javax.validation.constraints.DecimalMax;
/**
 *
 * @author apprentice
 */
public class ProductWith {
    
//    @NotEmpty(message="You must supply us with money.")
    @DecimalMax("50.00")
    private double amount;

    Product p;

    public ProductWith() {
    }

    public Product getP() {
        return p;
    }

    public void setP(Product p) {
        this.p = p;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return p.getPrice();
    }

    public void setPrice(double price) {
        p.setPrice(price);
    }

    public int getInventory() {
        return p.getInventory();
    }

    public void setInventory(int inventory) {
        p.setInventory(inventory);
    }
    
    public String getName() {
        return p.getName();
    }

    public void setName(String name) {
        p.setName(name);
    }
}
